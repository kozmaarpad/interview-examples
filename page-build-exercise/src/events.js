function reportEvent(event) {
    event.preventDefault();

    if (reportEvent) {
        alert(`${this.successMessage}${event.target.attributes.href.value}`);
    } else {
        alert(this.errorMessage);
    }
}

function buildEvents(shouldReportEvent) {
    this.links = document.querySelectorAll('.action-item a');
    this.reportEvent = shouldReportEvent;

    this.links.forEach(link => {
        link.addEventListener('click', reportEvent, {
            once: false,
        });
    });
}
