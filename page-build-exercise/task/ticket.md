# Bug #842

**Repro Steps**

1. open the blog list page
2. click on any "Read more..." link

**Actual Result**

Nothing happens.

**Expected Result**

Got an alert notification with a text "Button clicked successfully:" and the proper id of the clicked post item.<br>
Then I've got redirected to the anchored link.


**Attachments**

![](bug_shot.PNG)
