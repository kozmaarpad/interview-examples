module.exports = {
  clearMocks: true,
  testEnvironment: "node",
  setupFilesAfterEnv: ['./jest.setup.js'],
};
