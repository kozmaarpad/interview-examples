_name = 'bar';

(
    () => new Promise((resolve, reject) => {
        const testObject = {
            _name: "foo",
            getObjectName: () => this._name,
            setObjectName: (value) => {
                this._name = value
            }
        };

        setTimeout(() => {
            testObject.setObjectName('bar');
            resolve(testObject.getObjectName());
        }, 500);

        console.log(testObject.getObjectName());
    })
)()
    .then(function(value) {
        console.log(value);
    });
