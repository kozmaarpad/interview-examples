class ComponentBase {
    constructor(templateId) {
        this.elem = document.getElementById(templateId).cloneNode(true);
        this.elem.classList.toggle('hidden');
    }

    get domElem() {
        return this.elem;
    }
}

class QuestionComponent extends ComponentBase {
    constructor(model) {
        super('q-item-template');
        
        this.model = model;
    }
}

class ListContainerComponent extends ComponentBase {
    constructor(model) {
        super('q-list-template');
        
        this.children = new Array();
        
        this.model = model;
        this.model.forEach(qmodel => this.add(new QuestionComponent(qmodel)));
    }

    add(component) {
        this.children.push(component);
        this.elem.appendChild(component.elem);
    }

    remove(component) {
        this.elem.removeChild(component);
    }
}