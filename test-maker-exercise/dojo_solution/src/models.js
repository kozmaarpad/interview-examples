class ModelBase {
    constructor() {

    }
}

class QuestionModel extends ModelBase {
    constructor(data) {
        this.question = data.question;
        this.answer = data.answer || '';
        this.created_at = data.created_at || Date.now();
        this.rate_stars = data.rate_stars || 0;
        this.element = null;
    }

}
